import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FoodItemComponent } from './food-item/food-item.component';
import { MenuComponent } from './menu/menu.component';
import {FormsModule} from "@angular/forms";
import { OrderComponent } from './order/order.component';

@NgModule({
  declarations: [
    AppComponent,
    FoodItemComponent,
    MenuComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
