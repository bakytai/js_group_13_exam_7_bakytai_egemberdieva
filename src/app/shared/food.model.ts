export class Food {
  constructor(
    public name: string,
    public price: number,
    public count: number,
    public iconUrl: string,
  ) {
  }
}
