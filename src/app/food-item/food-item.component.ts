import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Food} from "../shared/food.model";

@Component({
  selector: 'app-food-item',
  templateUrl: './food-item.component.html',
  styleUrls: ['./food-item.component.css']
})
export class FoodItemComponent {
  @Input() food!: Food;
  @Output() add = new EventEmitter();

  addFood() {
    this.add.emit();
  }
}
