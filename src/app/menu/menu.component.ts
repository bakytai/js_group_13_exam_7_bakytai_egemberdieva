import {Component, Input} from '@angular/core';
import {Food} from "../shared/food.model";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  @Input() food!: Food;
  carts = [
    {
      name: 'Hamburger',
      price: 80,
      count: 0,
      iconUrl: 'https://mcdonalds.ru/resize/-x1020/upload/iblock/776/0000_Hamburger_BB_1500x1500_min.png'
    },
    {
      name: 'Cheeseburger',
      price: 90,
      count: 0,
      iconUrl: 'https://mcdonalds.ru/resize/-x1020/upload/iblock/3ab/0000_Cheeseburger_BB_1500x1500_min.png'
    },
    {
      name: 'Fries',
      price: 45,
      count: 0,
      iconUrl: 'https://mcdonalds.ru/resize/500x500//upload/iblock/458/3010_Fries_medium_1500x1500_min.png'
    },
    {
      name: 'Coffee',
      price: 70,
      count: 0,
      iconUrl: 'https://mcdonalds.ru/resize/500x500//upload/iblock/c8c/7188_Americano04_1500x1500_min.png'
    },
    {
      name: 'Tea',
      price: 50,
      count: 0,
      iconUrl: 'https://mcdonalds.ru/upload/iblock/986/7145_Ceylon03_1500x1500_min.png'
    },
    {
      name: 'Cola',
      price: 40,
      count: 0,
      iconUrl: 'https://mcdonalds.ru/resize/500x500//upload/iblock/f1a/7164_ColaZero_brand_1500x1500_2_min.png'
    },
  ];
  menu: Food[] = [];
  orderArray: any[] = [];
  totalSum = 0;



  constructor() {
    this.carts.forEach(food => {
      this.menu.push(new Food(food.name, food.price, food.count, food.iconUrl));
    })
  }

  addFoodItem(index: number) {
    this.orderArray.push({name: this.menu[index].name,count: 0, price: this.menu[index].price})
    console.log(this.orderArray);
    
  }

  onDelete(index: number) {
    return  this.orderArray = this.orderArray.filter(item => item.name === this.menu[index].name);
  }
}
